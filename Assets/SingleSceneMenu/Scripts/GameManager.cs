﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour
{
    public Menu InitialMenu;
    public Menu CurrentMenu;

    public TMenu OpenMenu<TMenu>(TMenu view) where TMenu : Menu
    {
        if (CurrentMenu)
            Destroy(CurrentMenu.gameObject);
        TMenu instance = Instantiate(view);
        instance.GameManager = this;
        CurrentMenu = instance;
        return instance;
    }

    public void CloseMenu()
    {
        if (CurrentMenu)
            Destroy(CurrentMenu.gameObject);
    }

    private void Start()
    {
        OpenMenu(InitialMenu);
    }
}
