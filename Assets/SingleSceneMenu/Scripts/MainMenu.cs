﻿using UnityEngine;
using System.Collections;

public class MainMenu : Menu
{
    public CreditsMenu CreditsMenu;

    public void OpenCredits()
    {
        CreditsMenu instance = (CreditsMenu)GameManager.OpenMenu(CreditsMenu);
    }

    public void OpenCredits(string text)
    {
        CreditsMenu instance = (CreditsMenu)GameManager.OpenMenu(CreditsMenu);
        instance.Header = text;
    }

    public void Close()
    {
        GameManager.CloseMenu();
    }
}
