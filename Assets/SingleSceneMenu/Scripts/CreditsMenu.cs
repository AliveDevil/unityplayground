﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.UI;

public class CreditsMenu : Menu
{
    public MainMenu Menu;
    public string Header = "test";
    public Text Text;

    private void Start()
    {
        Text.text = Header;
    }

    public void BackToMainMenu()
    {
        GameManager.OpenMenu(Menu);
    }
}
