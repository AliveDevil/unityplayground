﻿using UnityEngine;

public class CubeMovement : MonoBehaviour
{
    [SerializeField]
    private new Rigidbody rigidbody;
    [SerializeField]
    private float slipFactor = 1;
    public Vector3 Direction { get; set; }
    public Vector3 OldDirection { get; set; }
    public float OldVelocity { get; set; }

    public float SlipFactor
    {
        get { return slipFactor; }
        set { slipFactor = value; }
    }

    public float Velocity { get; set; }

    public void FixedUpdate()
    {
        OldDirection = Direction;
        OldVelocity = Velocity;

        Direction = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        Velocity = Direction.magnitude * 125;
        Direction.Normalize();

        Direction = Vector3.Slerp(OldDirection, Direction, SlipFactor);
        Velocity = Mathf.Lerp(OldVelocity, Velocity, SlipFactor);

        // Fix: Lerp Velocity.
        rigidbody.velocity = Direction * Velocity * Time.fixedDeltaTime;
    }
}
