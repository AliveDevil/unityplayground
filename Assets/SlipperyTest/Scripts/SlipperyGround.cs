﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SlipperyGround : MonoBehaviour
{
    [SerializeField]
    private float SlipFactor;
    private new List<Collider> collider = new List<Collider>();

    public void OnTriggerEnter(Collider other)
    {
        other.GetComponent<CubeMovement>().SlipFactor = SlipFactor;
        collider.Add(other);
    }

    public void OnTriggerExit(Collider other)
    {
        other.GetComponent<CubeMovement>().SlipFactor = 1;
        collider.Remove(other);
    }

    public void OnDestroy()
    {
        foreach (var c in collider)
        {
            c.GetComponent<CubeMovement>().SlipFactor = 1;
        }
    }
}
